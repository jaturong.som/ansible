# Ansible 

## Requirements

- Ansible installed on local machine. But this time, we will use docker-compose on one ec2 to setup all environments
- To use Ansible interact with AWS Infrastructure we need to install also python3 and boto3

## LAB Environment
```
AWS account : vfcx
AWS region : seoul
Jenkins internal : http://52.78.204.252:8080
Jenkins user :  admin/admin@123
mysql root pwd : cxzR%qVJy2*dmT
```
## Diagram

![Cat](./ansible-demo.jpg)
